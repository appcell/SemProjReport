\chapter[Second-Order Cut-Cell Method]{Second-Order Cut-Cell Method}

First-order projection sets pressure value of each cut-cell at the center of underlying regular cell. Although it brings convenience to pressure gradient calculation, it is physically incorrect and could cause errors. To fix the issues of first-order projection, cut-cell pressure values have to be moved to its own geometrical center, ensuring physical correctness and further bringing more accuracy. Based on this approach, Ji et.al. proposed another projection scheme with 2nd-order accuracy\cite{ji2006efficient}. Comparing with first-order pressure projection, the major difference lies in evaluation of flux through a certain sub-face according to Equation \ref{eq:pressureflux}.

\section[Pressure Projection with 2nd-Order Accuracy]{Refined Pressure Projection with 2nd-Order Accuracy}
The second-order scheme is more precise on fraction calculation. Taking Figure \ref{fig:cutcellsopressuresample} as an example. To compute elements in Poisson matrix for the configuration shown for sub-cell $sc_1$, $F_e$ would be written as,
\begin{figure}[!htb]
	\centering
	\subfigure[First-order pressure projection.]
	{\label{fig:cutcellsopressuresample1}
		\includegraphics[width=0.45\textwidth]{figures/cutcellsopressuresample1}} \hfill
	\subfigure[Second-order pressure projection.] {\label{fig:cutcellsopressuresample2}
		\includegraphics[width=0.45\textwidth]{figures/cutcellsopressuresample2}}
	\caption[Different pressure projection methods for Cut-Cell grid.]{Different pressure projection methods for Cut-Cell grid. Blue dots: center of regular cells, also where pressure value is defined. Red dots: geometrical center of cut-cells. \ref{fig:cutcellsopressuresample1}: First-order pressure projection. During fraction $A_e$ calculation for cell $sc_1$, since pressure of $sc_4$ is defined at regular cell center, corresponding element in Poisson matrix is $\frac{A_e}{\Delta x}$, which is physically incorrect. \ref{fig:cutcellsopressuresample2}: With 2nd-order modification}
	\label{fig:cutcellsopressuresample}
\end{figure}
\begin{equation}
F_e = \frac{A_e}{V_k}(\vec{\nabla p})_e \cdot \mathbf{n}_e \approx \frac{A_e}{\Delta x}(p_{e} - p_{k})
\end{equation}

while $A_e$ represents the edge connecting cell $sc_1$ and $sc_2$. In the first-order case, $\Delta x$ is the distance between two pressure definitions. Since pressure is only defined at regular cell centers, pressure gradient $\nabla p $ of each sub-cell is thus discretized as $(p_{e} - p_{k})/\Delta x$, resulting in inaccuracy in gradient approximation.

For the second-order approximation, we need a better pressure definition at more precise locations. In \ref{fig:cutcellsopressuresample2}, red dots are the geometric centers of cut-cells. To achieve better accuracy, auxiliary nodes $P'$ and $E'$ are generated as follows: first, we find the mid-point of connecting sub-face $A_e$ and mark it as point $e'$. $P'$ and $E'$ are then defined as the projection of sub-cell center $P$ and $E$ on the face normal of $A_e$. Instead of using $\Delta x$ for all pressure discretization, here we use $P'E'$ as the distance between two pressure definitions. Hence the flux $F_e$ becomes
\begin{equation}
F_e = \frac{A_e}{V_k}(\vec{\nabla p})_e \cdot \mathbf{n}_e \approx \frac{A_e}{{x}_{E'} - {x}_{P'}}(p_{E'} - p_{P'}).
\end{equation}
Pressure values at $P'$ and $E'$ are evaluated by interpolation from pressure at $P$ and $E$, that is
\begin{gather}
p_{P'} = p_{P} + \frac{\partial p}{\partial y}\bigg|_{P}(y_{P'} - y_{P}) \\
p_{E'} = p_{E} + \frac{\partial p}{\partial y}\bigg|_{E}(y_{E'} - y_{E})
\end{gather}

Note here according to Equation \ref{eq:cutcellpressure}, since only normal component of pressure gradient is taken into consideration, location changes in tangential directions are neglected, thus comes $P'$ and $E'$ being projection only on normal direction. Combining with pressure interpolation, the flux through face $A_e$ is finally approximated as

\begin{equation}
F_e \approx \frac{A_e(p_E - p_P)}{{x}_{E'} - {x}_{P'}} + \frac{A_e}{{x}_{E'} - {x}_{P'}}\left(\frac{\partial p}{\partial y}\bigg|_{E}(y_{E'} - y_{E}) - \frac{\partial p}{\partial y}\bigg|_{P}(y_{P'} - y_{P})\right)
\end{equation}

For all the 4 directions together, as Figure \ref{fig:cutcellsopressurefullsample} illustrates, we reach the evaluation of total flux in one sub-cell, also the left-hand side of Poisson equation,
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/cutcellsopressurefullsample}
	\caption[A sample cut-cell mesh.]{A sample cut-cell mesh. Blue dots: center of regular cells. Black dots: center of cut-cells. Red-dots: auxiliary nodes for pressure gradient computation. }
	\label{fig:cutcellsopressurefullsample}
\end{figure}
\begin{equation} \label{eq:cutcellsolhs_old}
\begin{split}
&\iiint_{V_{k}} \nabla \cdot \vec{\nabla p} \; d V = \oint_{S_{k}} \vec{\nabla p} \cdot \mathbf{n} \; d V = F = F_e + F_w + F_n + F_s \\
&\approx \frac{A_e(p_E - p_P)}{{x}_{E'} - {x}_{P}} + \frac{A_e}{{x}_{E'} - {x}_{P}}\left(\frac{\partial p}{\partial y}\bigg|_{E}(y_{e'} - y_{E}) - \frac{\partial p}{\partial y}\bigg|_{P}(y_{e'} - y_{P})\right) \\
&- \frac{A_w(p_W - p_P)}{{x}_{W'} - {x}_{P}} - \frac{A_e}{{x}_{W'} - {x}_{P}}\left(\frac{\partial p}{\partial y}\bigg|_{W}(y_{w'} - y_{W}) - \frac{\partial p}{\partial y}\bigg|_{P}(y_{w'} - y_{P})\right) \\
&+ \frac{A_n(p_N - p_P)}{{y}_{N'} - {y}_{P}} + \frac{A_e}{{y}_{N'} - {y}_{P}}\left(\frac{\partial p}{\partial x}\bigg|_{N}(x_{n'} - x_{N}) - \frac{\partial p}{\partial x}\bigg|_{P}(x_{n'} - x_{P})\right) \\
&- \frac{A_s(p_S - p_P)}{{y}_{S'} - {y}_{P}} - \frac{A_e}{{y}_{S'} - {y}_{P}}\left(\frac{\partial p}{\partial x}\bigg|_{S}(x_{s'} - x_{S}) - \frac{\partial p}{\partial x}\bigg|_{P}(x_{s'} - x_{P})\right).
\end{split}
\end{equation}

And corresponding right-hand side coefficients are,
\begin{equation}
d = A_e u_{e} - A_w u_{w} + A_n v_{n} - A_s v_{s}.
\end{equation}

To preserve the original structure of Poisson matrix, we move all pressure derivatives in Equation \ref{eq:cutcellsolhs_old} to the right-hand side. The final discretized Poisson Equation for 2nd-order cut-cell grid is given by,
\begin{gather}
\begin{split}\label{eq:cutcellsolhs_new}
\mathtt{LHS} &= F_e + F_w + F_n + F_s \\
&\approx \frac{A_e(p_E - p_P)}{{x}_{E'} - {x}_{P}}
- \frac{A_w(p_W - p_P)}{{x}_{W'} - {x}_{P}}
+ \frac{A_n(p_N - p_P)}{{y}_{N'} - {y}_{P}}
- \frac{A_s(p_S - p_P)}{{y}_{S'} - {y}_{P}} \\
&= C_E p_E + C_W p_W + C_N p_N + C_S p_S - (C_E + C_W + C_N + C_S) p_P,
\end{split}\\
\begin{split}
C_E = +\frac{A_e}{{x}_{E'} - {x}_{P}}, C_W = -\frac{A_w}{{x}_{W'} - {x}_{P}},
C_N = +\frac{A_n}{{y}_{N'} - {y}_{P}}, C_S = -\frac{A_s}{{y}_{S'} - {y}_{P}}.
\end{split}\\
\end{gather}
And,
\begin{gather}
\begin{split}\label{eq:cutcellsorhs}
\mathtt{RHS} &= A_e u_{e} - A_w u_{w} + A_n v_{n} - A_s v_{s} \\
&- \frac{A_e}{{x}_{E'} - {x}_{P}}\left(\frac{\partial p}{\partial y}\bigg|_{E}(y_{e'} - y_{E}) - \frac{\partial p}{\partial y}\bigg|_{P}(y_{e'} - y_{P})\right) \\
&+ \frac{A_w}{{x}_{W'} - {x}_{P}}\left(\frac{\partial p}{\partial y}\bigg|_{W}(y_{w'} - y_{W}) - \frac{\partial p}{\partial y}\bigg|_{P}(y_{w'} - y_{P})\right) \\
&- \frac{A_n}{{y}_{N'} - {y}_{P}}\left(\frac{\partial p}{\partial x}\bigg|_{N}(x_{n'} - x_{N}) - \frac{\partial p}{\partial x}\bigg|_{P}(x_{n'} - x_{P})\right) \\
&+ \frac{A_s}{{y}_{S'} - {y}_{P}}\left(\frac{\partial p}{\partial x}\bigg|_{S}(x_{s'} - x_{S}) - \frac{\partial p}{\partial x}\bigg|_{P}(x_{s'} - x_{P})\right).
\end{split}
\end{gather}

\section[Divergence Calculation]{Divergence Calculation}
During divergence calculation in Equation \ref{eq:cutcellsorhs}, it might worth notice that pressure gradient at sub-cell centers, e.g. $(\partial p /\partial y)|_{E}$ is not known. The solution to this issue is to use least-square, approximating the gradient at given location from points nearby.

\begin{figure}[!htb]
	\centering
	\subfigure[Cut-cell with 2 faces connecting with same phase.] {\label{fig:pic1}\includegraphics[width=0.3\textwidth]{figures/cutcell3nbs}} \hfill
	\subfigure[Cut-cell with 3 faces connecting with same phase.] {\label{fig:pic2}\includegraphics[width=0.3\textwidth]{figures/cutcell4nbs}} \hfill
	\subfigure[Cut-cell with 4 faces connecting with same phase.] {\label{fig:pic3}\includegraphics[width=0.3\textwidth]{figures/cutcell5nbs}}
	\caption[Cut-cell samples with different face number in the same phase.]{Cut-cell samples with different face number in the same phase.}
	\label{fig:differentcutcellnbs}
\end{figure}

Consider a cut-cell and its directly neighboring cells within same phase, as illustrated in \ref{fig:differentcutcellnbs}. A cut-cell can have 2, 3 or 4 regular or irregular neighboring cells within the same phase. Given a cell with its geometric center $P$ and one of its neighboring cells $j$, if pressure gradient $(\nabla p)_P$ at $P$ is exact, then pressure at $j$ can be expressed as,

\begin{equation}
p_j = p_P + (\vec{\nabla p})_P \cdot (\mathbf{r}_j - \mathbf{r}_P),
\end{equation}

with $\mathbf{r}_j$ denoting location of center of sub-cell $j$. However since pressure field is usually non-linear, the gradient $(\nabla p)_P$ cannot be exact. To search for a better expression of $(\nabla p)_P$, we define a loss function:

\begin{equation}
E = \sum_{j} w_j \left( (\vec{\nabla p})_P \cdot (\mathbf{r}_j- \mathbf{r}_P) - (p_j - p_P) \right).
\end{equation}

This loss function defines a least-square optimization problem. Its solution is given by a linear system,
\begin{equation} \label{eq:pressuregrad}
\begin{bmatrix}
\sum_{j} w_j \Delta x_j \Delta x_j & \sum_{j} w_j \Delta x_j \Delta y_j\\
\sum_{j} w_j \Delta x_j \Delta y_j & \sum_{j} w_j \Delta y_j \Delta y_j
\end{bmatrix}
\begin{bmatrix}
\nabla p_{Px} \\
\nabla p_{Py}
\end{bmatrix} =
\begin{bmatrix}
\sum_{j} w_j \Delta x_j \Delta p_j \\
\sum_{j} w_j \Delta x_j \Delta p_j
\end{bmatrix},
\end{equation}

where
\begin{equation}
\Delta x_j = x_j - x_p, \Delta y_j = y_j - y_p, \Delta p_j = p_j - p_p.
\end{equation}

The least-square interpolation successfully simulated pressure gradient in \cite{ji2006efficient} when the authors tried to interpret pressure of one fluid phase from the other, bringing second order accuracy into pressure projection. However, when dealing with fluid-solid coupling instead of fluid-fluid coupling, the least-square approach might generate extra interpolation error. This is due to the fact that in fluid-solid interaction, fluid pressure and velocity on the solid side is always zero, thus has no contribution to boundary cells, resulting in an extremely unbalanced interpolation. Especially for a triangular cut-cell (cut-cell with 2 faces connecting to a same phase), since the centroids of its 2 supporting cells are on the same phase forming a straight line, interpolation to a third point away from the line can be random, causing uncorrectable numerical errors. Difference between fluid-fluid and fluid-solid coupling has a high probability to destroy the simulation after only a few iterations.

To solve this issue, we replaced least-square with Mean Value Coordinate(MVC) based interpolation. The new algorithm to compute pressure gradient at boundary cells works as follows: we assume a No-Slip boundary condition for fluid-solid coupling, i.e. fluid velocity at mixed and solid nodes in Figure \ref{fig:freeslip} is directly assigned as the velocity of solid. Then separated interpolation is applied to grid nodes and cut-cell centroids.

For grid nodes, since in 2D space a grid node always has 4 faces connected, its pressure gradient can be naturally inferred from the 4 direct supporters. As shown in \ref{fig:mvcgridnode1}, to calculate pressure gradient at the grid node (blue dot in the figure), first we calculate the pressure gradient along one line connected by centroids of its two neighboring supporters (the centroids are marked by green dots, and the currently referenced neighboring faces are marked with light green). Since pressure values at the centroid of each face are given, it's easy to calculate gradient along the direction formed by face centroids, then we assume gradient value along the line is same as that at the projection of original grid node on the line. That is, we assume pressure gradient on the line is equal to gradient at the red node. Repeat this procedure for all 4 pairs of neighboring faces, we get 4 red nodes forming a polygon (marked red in Figure \ref{fig:mvcgridnode2}).

Then, given pressure gradient values on each node of this polygon, we do a MVC based interpolation, as shown in Figure \ref{fig:mvcgridnode3}, to get pressure gradient value on a grid node.

\begin{figure}[!htb]
	\centering
	\subfigure[Get pressure gradient of one edge formed by centroids of 2 neighboring supporting faces(marked in light green). Then use the gradient as gradient on the red node. ] {\label{fig:mvcgridnode1}\includegraphics[width=0.3\textwidth]{figures/mvcgridnode1}} \hfill
	\subfigure[Projections of the gird node on lines connecting face centoids form a polygon, marked in red.] {\label{fig:pic2}\includegraphics[width=0.3\textwidth]{figures/mvcgridnode2}} \hfill
	\subfigure[Interpolate pressure gradient value on grid node from the 4 projections.] {\label{fig:mvcgridnode3}\includegraphics[width=0.3\textwidth]{figures/mvcgridnode3}}
	\caption[MVC-based pressure gradient calculation on grid nodes.]{MVC-based pressure gradient calculation on grid nodes. Green dots: centoids of faces. Blue dot: the grid node on which we interpolate pressure gradient value. Red dot: the projection of blue grid node onto one edge formed by centroids of two neighboring faces.}
	\label{fig:mvcgridnode}
\end{figure}


With gradients know on grids nodes, calculation of gradient on cut-cell centroids is simpler: no matter how many faces in the same fluid phase one cut-cell connects with, pressure gradient on its center can always be interpolated from 3 or more nodes forming the polygonal face. For example in a triangular cut-cell illustrated in \ref{fig:mvcfacecenter}, gradient on the green grid node is known from previous calculations, and gradient on red solid nodes are set to be zero from No-Slip condition. With a similar MVC, pressure gradient on cut-cell centroids are interpolated without randomized numerical errors generated by least-square. With this algorithm we managed to avoid instability in divergence calculation within small cut-cells.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\textwidth]{figures/mvcfacecenter}
	\caption[Pressure gradient interpolationon on cut-cell centroid from nodes of the underlying polygonal cut-cell.]{Pressure gradient interpolation on cut-cell centroid from nodes of the underlying polygonal cut-cell. Blue dot: centroid of one cut-cell. Green dot: grid node. Red dot: solid nodes marking fluid-solid interface. According to No-Slip condition, pressure gradient on red nodes is always zero. }
	\label{fig:mvcfacecenter}
\end{figure}


With all the refinement added, the modified cut-cell scheme can be summarized as Algorithm \ref{alg:cutcellso} shown below.

\begin{algorithm}
	\caption[Second-order Cut-Cell]{Algorithm for second-order Cut-Cell solver with zero-viscosity incompressible fluid}
	\label{alg:cutcellso}
	\begin{algorithmic}
		\REQUIRE an initial velocity field $\mathbf{u}^0$ that is divergence-free
		\WHILE{$t$ < $T$}
		\STATE \textbf{Advection}:
		\STATE Reconstruct velocities at nodes/edges as Figure \ref{freeslip}
		\STATE Get $\mathbf{u'}^{n}$ from solving advection: $\frac{\partial \mathbf{u}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{u} = 0$
		\STATE \textbf{Add body force}:
		\STATE $\mathbf{u'}^{n} \Leftarrow \mathbf{u'}^{n} + \mathbf{f} \Delta t $
		\STATE \textbf{Pressure projection}:
		\STATE Compute gradient at grid nodes with MVC
		\STATE Compute gradient at cut-cell centroids with MVC
		\STATE Compute divergence for RHS in Poisson equation, following Equation \ref{eq:cutcellsorhs}
		\STATE Compute LHS in Poisson equation, following Equation  \ref{eq:cutcellsolhs_new}
		\STATE Solve Poisson equation, get pressure field $p$
		\STATE Add pressure projection: $\mathbf{u}^{n+1} \Leftarrow \mathbf{u'}^{n} - \Delta t \frac{1}{\rho} \nabla p$
		\STATE \textbf{Time-stepping}:
		\STATE $t \leftarrow t+1$
		\ENDWHILE
	\end{algorithmic}
\end{algorithm} 

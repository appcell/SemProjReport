% set counter to n-1:
\setcounter{chapter}{1}

\chapter{Related Works}

\section{Grid-based and Particle-based Fluid Simulations}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/sph}
	\caption[Smoothed Particle Hydrodynamics(SPH).]{Smoothed Particle Hydrodynamics(SPH). SPH calculates all macro-properties in fluid with particle distributions, smoothened by a short-distance kernel function over neighborhood. The kernel should be smooth, describing quantity distribution near the center particle. }
	\label{fig:sph}
\end{figure}

Numerically solving non-linear PDEs as Navier-Stokes equations requires a discrete representation for the simulation domain. There are two different paradigms for discretization: the Eulerian (grid-based) and Lagrangian (particle-based) viewpoints. Grid-based methods (\Fig{eulerian}) store velocity, pressure and additional quantities describing flow properties on grid-cells. Eulerian approach ensures the smoothness of PDE solution, while it also enables a fast and easy neighbor search for further dealing with fluid motions or interaction with meshes. For its scalability, Eulerian methods became widely popular.

\begin{figure}[!htb]
	\centering
	
	\subfigure[Grid-based Eulerian approach. Grids are arranged at fixed spatial locations, flow-related quantities are measured at grid nodes when fluid flows past.] {\label{fig:eulerian}\includegraphics[width=0.45\textwidth]{figures/eulerian}} \hfill
	\subfigure[Particle-based Lagrangian approach. Particles represent small chunks of fluid, carries flow-related quantities during motions. Fluid motions are implemented by particle moving.]{\label{fig:lagrangian}\includegraphics[width=0.45\textwidth]{figures/lagrangian}}
	
	\caption[Different approaches of fluid simulation]{Different approaches of fluid simulation.}
	
	\label{fig:spatialdiscretization}
\end{figure}

An innovation of early-stage Eulerian approach is Marker-and-Cell (MAC) grid introduced by Harlow and Welch \cite{harlow1965numerical}. Instead of storing all flow quantities at cell centers, the MAC grid is a "staggered" grid which stores velocity components on faces of cells, while keeping pressure and other flow properties at cell centers. As shown in \ref{fig:macdiscretization2}, central differences can be computed with robust second-order accuracy, resulting in a more accurate and stable computation of pressure gradient and velocity divergence. For evaluating velocity at continuous locations, interpolation is required.

Another paradigm for spatial discretization is particle-based Lagrangian methods, which emphasizes particle motion and transport. Instead of storing fluid properties in a grid, Lagrangian methods simulate flow motions by modeling small chunks of fluid through particle systems. Compared with Eulerian approaches, Lagrangian methods work better at dealing with transport; additionally splash and droplets formation is better modeled on a Lagrangian framework. On the other hand, particles are not efficient at ensuring divergence-freeness. Also neighbor search can be a  computational liability when calculating averaged quantities like density and pressure. Smoothed Particle Hydrodynamics(SPH) \cite{monaghan1992smoothed} is one of the fundamental Lagrangian approaches. SPH calculates all macro-properties in fluid with particle distributions, continuously calculated by a short-distance kernel function over neighborhood. Thus, kernels describe the quantity distribution around a particle center.

Pure grid-based approaches are better at enforcing mass conservation than Lagrangian methods thus they were a popular choice in Computer Graphics applications. However, they have difficulties tracking fluid movements associated with the transport of quantities. Thus,  "hybrid" methods were proposed, combining the power of both the particle and grid perspectives. The PIC/FLIP methods proposed by Zhu et.al \cite{zhu2005animating} is a combination of Particle-in-Cell(PIC) \cite{harlow1962particle} and Fluid-
Implicit-Particle(FLIP) \cite{brackbill1986flip} used to model a continuous Sand model in a fluid setting. In each frame, particle attributes such as velocities are projected onto grid cells, then forces and pressures are computed on grid cells based on averaged particle velocities. After that, velocities get updated and re-interpolated back on particles . With a hybrid method, advection is treated as local particle-based movements, while mass conservation can be easily calculated by solving a linear system with finite differences. A more detailed explanation is given in Chapter 3.


\section{Variation of Grids Dealing with Thin Boundaries}

Various methods have been developed to study the dynamics of multi-phase fluid or fluid-solid interaction. The main difficulty for these computations is inevitably the treatment of interface, especially when geometry becomes complex or sometimes varies over time. The correct treatment of boundary conditions is paramount for proper discretization of high frequency features that appear close to the boundary of interest. Zhang et al. used and adaptive grid formulation close to the object to proper capture high vorticity boundary layers, which greatly increase the richness of the simulated flow; however their approach still implies a voxelized boundary discretization, which introduces artifacts on modeling proper boundary conditions. Of most numerical methods dealing with interface problem in fluid simulation, Immersed boundary method becomes the origin of all grid-based approaches solving this challenge by representing boundaries with discrete Delta functions. To conform complex immersed bodies onto a grid system, many variations of Immersed Boundary Methods (IBM) \cite{peskin2002immersed} are developed.

One of practical application of Immersed Boundary Methods for single-phase fluid is from Y.Zhu et.al. \cite{zhu2002simulation}. Using a multi-grid discretization with a smooth approximation of Delta function over grid cells to represent mass density of immersed filament, they simulated motion of a thin flexible curve in 2D, partly treating it as fluid with higher density. However a smooth approximation in Immersed Boundary Method cannot handle the pressure jump between two sides of the filament, thus cannot be applied to solid-fluid interaction with signification pressure jump. In another work from E.Guendelman et.al. \cite{guendelman2005coupling}, the authors extended IBM further by putting analytic constraints on fluid close to object, making it possible to interact with rigid triangulated thin surface.

Another extension of IBM, Immersed Interface Methods(IIM) initially proposed by Z.Li et.al. \cite{li2001immersed}, made up for jump condition handling in IBM. Pressure jumps and velocity normal derivatives are directly derived from boundary force instead of approximated Delta functions, producing second-order accuracy at boundary cells. However, Immersed Interface Method is mostly limited to thin structures, and its requirement on object geometry is hard to meet.\cite{hou2012numerical}

Compared with Immersed Boundary Method and Immersed Interface Method, Cut-cell methods can not only preserve volume and jump conditions, but also hugely reduce the time needed when polishing a complex interface geometry. It applies an irregular surface into Cartesian grids by "cutting" a polygonal mesh out of predominant Cartesian grid, ensuring the mesh being structure-fitted while still able to merge into the background grid. The interface is usually represented by poly-lines, with starting and ending nodes being intersection points on grid cell walls. Similarly in 3D, surface can be defined using a conformal polygonal discretization. Most polygonal-based representations are suitable for a cut-cell implementation.

One example of cut-cell application in graphics is from V.Azevedo et.al. \cite{azevedo2016preserving}. This paper introduces a topologically-accurate discretization of pressure projection with cut-cell settings, handling both thin obstacles and narrow gaps with one symmetric Poisson matrix. In addition, an improvement of velocity interpolation for cut-cells based on Lagrangian PIC/FLIP particles is proposed. This work reaches first order accuracy at boundary cut-cells, while another paper by H.Ji et.al. \cite{ji2006efficient} proposed a more precise approach. With a finer pressure discretization, they evaluated flux across fluid-fluid interface with second order accuracy, leading to a finer estimation of pressure. However their approach doesn't fully suit fluid-solid interaction, which is the problem to be studied in this report.

errors = [];
errors_so = [];
dist = [0.125; 0.0625; 0.03125; 0.015625; 0.0078125; 0.00390625];
distStr = ["0.125"; "0.0625"; "0.03125"; "0.015625"; "0.0078125"; "0.00390625"];

for tempD = 1:size(dist, 1)
    currD = distStr(tempD);
    M = csvread(currD + "/pressure.csv");
    M_so = csvread(currD + "/pressure_so.csv");
    M_eval = csvread("0.03125/pressure_so.csv");
    evaluatedPoints = [];
    for i = 1:size(M_eval, 1)
        if M_eval(i, 1) ~= 0
            evaluatedPoints = [evaluatedPoints; M_eval(i, 2), M_eval(i, 3)];
        end
    end


    %% calculate error

    X = M(:, 2);
    Y = M(:, 3);
    Z = M(:, 1);

    X_so = M_so(:, 2);
    Y_so = M_so(:, 3);
    Z_so = M_so(:, 1);

    InterpHandle_so = scatteredInterpolant(X_so, Y_so, Z_so);
    InterpHandle = scatteredInterpolant(X, Y, Z);
    values = [];
    values_so = [];
    for i = 1:size(evaluatedPoints, 1)
        values = [values; InterpHandle(evaluatedPoints(i, 1), evaluatedPoints(i, 2))];
        values_so = [values_so; InterpHandle_so(evaluatedPoints(i, 1), evaluatedPoints(i, 2))];
    end

    tempError = 0;
    tempError_so = 0;
    for i = 1:size(evaluatedPoints, 1)
        tempError = tempError + abs(values(i) - values_ref(i))^2; 
        tempError_so = tempError_so + abs(values_so(i) - values_so_ref(i))^2; 
    end
    
    tempError = tempError / (size(evaluatedPoints, 1) - 1);
    tempError_so = tempError_so / (size(evaluatedPoints, 1) - 1);
    errors = [errors; tempError];
    errors_so = [errors_so; tempError_so];

end

loglog(dist, errors, dist, errors_so)
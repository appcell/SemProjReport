    M = csvread("pressure_0.003906.csv");
    M_so = csvread("pressure_so_0.003906.csv");
    M_eval = csvread("pressure_so_0.003906.csv");
    evaluatedPoints = [];
    for i = 1:size(M_eval, 1)
        if M_eval(i, 4) == 1 && M_eval(i, 1) ~= 0
            evaluatedPoints = [evaluatedPoints; M_eval(i, 2), M_eval(i, 3)];
        end
    end


    %% calculate error

    X = M(:, 2);
    Y = M(:, 3);
    Z = M(:, 1);

    X_so = M_so(:, 2);
    Y_so = M_so(:, 3);
    Z_so = M_so(:, 1);

    InterpHandle_so = scatteredInterpolant(X_so, Y_so, Z_so);
    InterpHandle = scatteredInterpolant(X, Y, Z);
    values_ref = [];
    values_so_ref = [];
    for i = 1:size(evaluatedPoints, 1)
        values_ref = [values_ref; InterpHandle(evaluatedPoints(i, 1), evaluatedPoints(i, 2))];
        values_so_ref = [values_so_ref; InterpHandle_so(evaluatedPoints(i, 1), evaluatedPoints(i, 2))];
    end




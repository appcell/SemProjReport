model = "simple_circle";
str = "Vorticity";
timestep = 80;
dist = 0.0625/2;
width = 4;
height = 2;
distInName = "0.031250";

M = csvread(model+"_t_" + timestep + "/"+lower(str)+".csv");
Mso = csvread(model+"_t_" + timestep + "/"+lower(str)+"_so.csv");
%M = csvread(model+"_t_" + timestep + "/"+lower(str)+"_"+distInName+".csv");
%Mso = csvread(model+"_t_" + timestep + "/"+lower(str)+"_so"+"_"+distInName+".csv");
X = M(:, 2);
Y = M(:, 3);
Z = M(:, 1);
Xso = Mso(:, 2);
Yso = Mso(:, 3);
Zso = Mso(:, 1);

minVal = min(min([Z,Zso]));
maxVal = max(max([Z,Zso]));
clims = [minVal, maxVal];

[Xi,Yi] = meshgrid(linspace(0,width,width/dist),linspace(0,height,height/dist));
Si = scatteredInterpolant(X,Y,Z);
Ci = Si(Xi,Yi);
figure;
subplot(2,1,1);
imagesc(Ci, clims);
colorbar;
title(str + " of original solver at timestep t="+timestep);
hold on;
contour(Ci,15, 'LineColor', 'k');



[Xiso,Yiso] = meshgrid(linspace(0,width,width/dist),linspace(0,height,height/dist));
Siso = scatteredInterpolant(Xso,Yso,Zso);
Ciso = Siso(Xiso,Yiso);
subplot(2,1,2);
imagesc(Ciso, clims);
colorbar;
title(str + " of modified solver at timestep t="+timestep);
hold on;
contour(Ciso,15, 'LineColor', 'k');
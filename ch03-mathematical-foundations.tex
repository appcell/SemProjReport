\chapter{Mathematical Foundations of Fluid Simulation}

\section{Navier-Stokes Equations}

Navier-Stokes equations are named after Claude-Louis Navier and George Gabriel Stokes, describing motions of viscous, isotropic and incompressible fluid. The equations are derived from applying Newton's second law to continuum mechanics concepts, based on the assumption that fluid is Newtonian. In Navier-Stokes equations, fluid pressure and viscosity form the force acting on fluid chunks; also, viscosity varies proportionally with velocity gradient. Replacing the viscosity coefficient with the viscosity tensor, the equations can be further extended to represent anisotropic fluids. Fluid motions are of the interest of many scientific and engineering fields, hence the Navier-Stokes equations have become the basis of most Newtonian fluid simulations.

The Navier-Stokes equations are explained as follows: A fluid with constant temperature and density can be described by a velocity field $\mathbf{u}$ and pressure field $p$. Given initial velocity $u_0$ and pressure $p_0$ at $t=0$, at any time $t>0$, status of both quantities are uniquely determined by Navier-Stokes equation:
\begin{equation} \label{eq:ns1}
\frac{\partial \mathbf{u}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{u} = - \frac{1}{\rho} \nabla p + \nu \Delta \mathbf{u} + \mathbf{f}.
\end{equation}
\begin{equation} \label{eq:ns2}
\nabla \cdot \mathbf{u} = 0
\end{equation}

The list of all variables and operators involved are given in Table \ref{tab:symbols}. Equations \ref{eq:ns1} and \ref{eq:ns2} form the Navier-Stokes equation system. Of all symbols appeared, there are a few exclusive terminology for fluid physics, e.g. kinetic viscosity $\nu$, measuring resistance to deformation during fluid motion. The higher $\nu$ is, the less deformation the fluid makes when flowing. Pitch is one example of fluid with high viscosity. Another quantity worth attention is $\mathbf{u}$, Velocity, a scalar field defined on each point in space, describing the velocity of fluid chunk moving pass the nearby region.

\begin{table}[!h]
	\centering
	\begin{tabular}{|l|p{0.4\linewidth}|}
		\hline
		\emph{Symbol} & \emph{Meaning}\\
		\hline
		$t$ &Time\\
		$p$ &Pressure field of fluid\\
		$\mathbf{u}$ &Velocity field of fluid\\
		$\rho$  &Density of fluid\\
		$\mathbf{f}$  &Body force, applied throughout fluid system, usually including gravity\\
		$\nu$ &Kinetic viscosity \\
		$p_0$ &Initial pressure\\
		$u_0$ &Initial velocity\\
		$\nabla$  &Gradient operator\\
		$\nabla \cdot$  &Divergence operator\\
		$\delta$  &Laplacian operator\\
		\hline
	\end{tabular}
	\caption[Symbols used in Navier-Stokes equations, Equation \ref{eq:ns1} and \ref{eq:ns2} ]{Symbols used in Navier-Stokes equations, \ref{eq:ns1} and \ref{eq:ns2}. \label{tab:symbols}}
\end{table}



The two equations emphasize on different physical conservation relations. Equation \ref{eq:ns1}, named as "Momentum Equation", is derived from momentum conservation which is further derived from Newton's 2nd law, $\mathbf{F}=m\mathbf{a}$. It consists of 3 parts: The left-hand side is fluid advection, applied to every fluid chunk. Advection on fluid includes all 3 forces on the right-hand side: force caused by fluid pressure from neighboring chunks $- \nabla p/{\rho} $, force caused by viscosity $\nu \Delta \mathbf{u}$, and body force $\mathbf{f}$ applied to all of the fluid.

Equation \ref{eq:ns2} comes from mass or volume conservation. Even though real fluid might change its volume slightly, for simulations, we assume the fluid doesn't change its volume at all, thus it satisfies incompressibility condition. For incompressible fluid, volume of an arbitrary chunk of fluid stays constant, i.e. rate of volume change is zero. Mathematically, it means the divergence on any region of fluid is zero, and we arrive at the Equation \ref{eq:ns2}. Throughout this report, only zero-viscosity fluid is taken into consideration, thus the viscosity term $\nu \Delta \mathbf{u}$ is always neglected.

Usually when dealing with fluid simulation, the difficulty lies in not only Navier-Stokes equations themselves, but also the boundary conditions. Typically we deal with two types of boundaries: when fluid collides with another fluid, and when fluid collides with solid surface, especially the latter. For fluid-solid interaction with fluid colliding into solid object, the fluid cannot flow into the solid , thus the normal of velocity at fluid-solid boundary is zero:

\begin{equation}
\mathbf{u} \cdot \mathbf{\hat{n}} = 0
\end{equation}

with $\mathbf{\hat{n}}$ denoting unit normal vector at boundary. However in the case when solid object is moving, fluid velocity at boundary has to also match that of interface.



\section{PIC/FLIP-based numerical solutions}

\subsection{Mathematical Overview}
As previously mentioned, Eulerian methods are widely used to numerically solve Navier-Stokes equations and simulate each variable at any given time-step. The main idea is to split whole domain into grid cells, then calculate all required quantities at fixed cellular locations. One advantage of Eulerian methods is, its grid-based discretization can easily compute Divergence and gradients with finite difference schemes. Quantities not located at measure points are calculated through interpolation.

The Eulerian approach works by splitting the complicated fluid equations into several separated parts, and solve them one after another. For the two Navier-Stokes equations, a basic Eulerian solver divides them into the following steps:

1. Advection:
\begin{equation}\label{eq:advection}
\frac{\partial \mathbf{q}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{q} = 0
\end{equation}

Equation \ref{eq:advection} is adapted from Equation \ref{eq:ns1}, but velocity field $\mathbf{u}$ is replaced with general quantity $q$, and the left-hand side material derivative is set to zero. Note here with the latter change, quantities carried by a certain particle will keep constant. To make up for the right-hand side, external forces will be added in following steps.

2. Body Force

\addComment{Add body forces equation here}

3. Pressure Projection
\begin{equation}\label{eq:pressureproj}
\frac{\partial \mathbf{q}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{q} = 0
\end{equation}

The 3 steps are executed in order during each time-step, as shown in Algorithm \ref{alg:euler}. At the beginning of time t, we assume the velocity field, $\mathbf{u}$, is divergence-free everywhere. Then advection, body force and diffusion(for zero-viscosity fluid, diffusion step can be neglected) are added in order, making $\mathbf{u}$ not divergence-free anymore. At last, a pressure force correction is made such that the flow becomes divergence free again at time $t+1$. A more detailed explanation of this algorithm is given in the following sections.

\begin{algorithm}
	\caption[Overview of Eulerian Approach]{Overview of Eulerian algorithm for zero-viscosity incompressible fluid}
	\label{alg:euler}
	\begin{algorithmic}
	\REQUIRE an initial velocity field $\mathbf{u}^0$ that is divergence-free
	\WHILE{$t$ < $T$}
	\STATE Advection: solve Equation \ref{eq:advection}
	\STATE Add body force: $\mathbf{u'}^{n} \Leftarrow \mathbf{u'}^{n} + \mathbf{f} \Delta t $
	\STATE Pressure projection
	\STATE Time-stepping: $t \leftarrow t+1$
	\ENDWHILE
	\end{algorithmic}
\end{algorithm}

\subsection{Grids}

To numerically solve these differential equations, a staggered grid is used for discretization. The grid has rectangle cells in 2D (or cubical cells in 3D). On a staggered grid, the variable sets $\mathbf{u}, p$ are defined at different locations: pressure $p$ is defined at cell center, and velocity components are defined at the center of cell faces. A 3D example of the grid discretization, and a 2D example of staggered grid are shown in Figure \ref{fig:macdiscretization}.

Why "staggered" grids? When calculating partial derivatives, an equidistant grid with all quantities defined on grid points would give wrong results due to the solution of central difference scheme not being unique. With pressure and velocity defined on staggering locations, pressure $p$ are shifted by half of a cell. Therefore when calculating derivatives, the differential operators use adjacent quantities in same cell instead of points on alternate cells, avoiding the so-called "pressure decoupling" problem. In the case of a staggered grid, extra interpolation is needed.

\begin{figure}[!htb]
	\centering
	
	\subfigure[3D grid system in PIC/FLIP discretization. The simulation space is divided into $2\times2\times2$ square grid cells.] {\label{fig:macdiscretization1}
		\includegraphics[width=0.45\textwidth]{figures/grid}} \hfill
	\subfigure[2D MAC grid used in PIC/FLIP scheme. Pressure $p$ is defined at cell center, and velocity components are defined at the center of cell faces. Here $u$ and $v$ are velocity components in different directions in velocity field $\mathbf{u}$. ]
	{\label{fig:macdiscretization2}
		\includegraphics[width=0.45\textwidth]{figures/macgrid}}
	
	\caption[MAC grid-based Eulerian discretization.]{MAC grid-based Eulerian discretization.}
	
	\label{fig:macdiscretization}
\end{figure}

\subsection{Advection}
In Algorithm \ref{alg:euler}, apart from pressure projection, another major step is solving advection equation \ref{eq:advection}. An obvious approach is to write out the PDE, then use a simple Forward Euler with finite difference. However this simplest approach becomes unacceptable in advection solving because of the unconditionally unstable nature of central difference.

A more physically-based approach inspired by Lagrangian particle perspective, so-called "Semi-Lagrangian", is proposed by J.Stam \cite{stam1999stable}. Different from Eulerian scheme, advection is simple with particles: value of quantity $q$ is carried by particles, thus to evaluate the quantity at location $\mathbf{x}$, we only need to find out the particles moved to $\mathbf{x}$, then get the quantities they carried. With the velocity field $\mathbf{u}$ known, we can easily figure out its original position by running backwards through velocity field then get old $q$ value from start point.

For example, if we want to calculate some quantity $q$ at a grid location $\mathbf{x}_G$. The updated value of $q$ at this location is $q^{n+1}_G$. However we know $q^{n+1}_G$ is just the old value $q^{n}_P$ of some particles originally at previous location $\mathbf{x}_P$. When a particle at $\mathbf{x}_P$ moves during $\Delta t$ and reaches $\mathbf{x}_G$, it carries old value $q^{n}_P$ to $\mathbf{x}_G$ and form $q^{n+1}_G$. The problem is then reduced to finding the particles originally at $\mathbf{x}_P$.

\begin{figure}[!htb]
	\centering
	\subfigure[To achieve the quantity $q$ at new location $\mathbf{x}_G$, we need to find the particle previously at $\mathbf{x}_P$ but arriving at $\mathbf{x}_G$ at time $n+1$. $\mathbf{x}_P$ is currently unknown. ] {\label{fig:pic1}\includegraphics[width=0.3\textwidth]{figures/pic1}} \hfill
	\subfigure[First step of semi-Lagrangian advection. Find velocity $\mathbf{u}_G$ at position $\mathbf{x}_G$ by interpolation from neighboring velocity storage.] {\label{fig:pic2}\includegraphics[width=0.3\textwidth]{figures/pic2}} \hfill
	\subfigure[Second step of semi-Lagrangian. With $\mathbf{u}_G$ known, one step of back-tracking from $\mathbf{x}_G$ gets us the original particle at  $\mathbf{x}_P$. From particle at $\mathbf{x}_P$ we get value of $q^{n+1}_G$.] {\label{fig:pic3}\includegraphics[width=0.3\textwidth]{figures/pic3}}
	\caption[Steps within a semi-Lagrangian iteration]{Steps within a semi-Lagrangian iteration.}
	\label{fig:picflip}
\end{figure}

Given velocity field, we can easily find out where $\mathbf{x}_P$ is, through back-tracing with current velocity at $G$:

$$\mathbf{x}_P = \mathbf{x}_G - \Delta t \cdot \mathbf{u}_G$$

Using staggered grid, velocity is defined at center of cell faces instead of center of cells. Thus for getting $\mathbf{u}_G$, interpolation is usually needed. Now we have the source location $\mathbf{x}_P$ calculated, next step is to find out $q^{n}_P$, the old $q$ value. $q^{n}_P$ is calculated by interpolation from particles nearby, since $\mathbf{x}_P$ isn't usually on the grid. The final step is to set the new value of $q_G^{n+1}$ at destination, with $q_G^{n+1} = q^n_p$. This advection scheme is not only used for velocity field, but also for other quantities like density.

\subsection{Pressure projection}

From a mathematical perspective, at time $t$, initially we have a divergence-free velocity field:

$$ \nabla \cdot \mathbf{u} = 0. $$

Then, after adding up advection, viscosity and body forces, we arrive at the intermediate velocity field $\mathbf{u'}^{n}$,

$$ \mathbf{u'}^n = f(\mathbf{u}^n). $$

If pressure correction is made, velocity field becomes,

\begin{equation} \label{eq:addpressure}
\mathbf{u}^{n+1} = \mathbf{u'}^n - \Delta t \frac{1}{\rho} \nabla p.
\end{equation}

However, here the exact value of pressure is not known yet. Since we have to ensure the initial velocity field is divergence-free, before time $t+1$, the divergence of $\mathbf{u}$ is 0:

\begin{equation} \label{eq:addpressure2}
\nabla \cdot \mathbf{u}^{n+1} = \nabla \cdot (\mathbf{u'}^n - \Delta t \frac{1}{\rho} \nabla p) = 0
\end{equation}

After rearranging, it becomes a Poisson equation with right-hand side already fully known,

\begin{equation} \label{eq:poisson}
\nabla \cdot \nabla p = \frac{\rho}{\Delta t} \nabla \cdot \mathbf{u'}^n.
\end{equation}

After calculating pressure from Equation \ref{eq:poisson}, we can finally add pressure correction onto velocity field with Equation \ref{eq:addpressure}, and arrive at a divergence-free velocity field for next iteration during time $t+1$. Putting all steps together, the basic Eulerian algorithm can be concluded with Algorithm \ref{alg:picflip}:

\begin{algorithm}
	\caption[Basic Eulerian]{Basic Eulerian algorithm for zero-viscosity incompressible fluid}
	\label{alg:picflip}
	\begin{algorithmic}
		\REQUIRE an initial velocity field $\mathbf{u}^0$ that is divergence-free
		\WHILE{$t$ < $T$}
		\STATE \textbf{Advection}:
		\STATE Get $\mathbf{u'}^{n}$ from solving advection: $\frac{\partial \mathbf{u}}{\partial t} + (\mathbf{u} \cdot \nabla) \mathbf{u} = 0$
		\STATE \textbf{Add body force}:
		\STATE $\mathbf{u'}^{n} \Leftarrow \mathbf{u'}^{n} + \mathbf{f} \Delta t $
		\STATE \textbf{Pressure projection}:
		\STATE Assemble Poisson equation: $\nabla \cdot \nabla p = \frac{\rho}{\Delta t} \nabla \cdot \mathbf{u'}^n$
		\STATE Solve Poisson equation, get pressure field $p$
		\STATE Add pressure projection: $\mathbf{u}^{n+1} \Leftarrow \mathbf{u'}^{n} - \Delta t \frac{1}{\rho} \nabla p$
		\STATE \textbf{Time-stepping}:
		\STATE $t \leftarrow t+1$
		\ENDWHILE
	\end{algorithmic}
\end{algorithm}

%For pressure updating, we need to solve the Poisson equation Eq.6, which includes three steps. First, compute the divergence of velocity $\mathbf{u}$. Second, solve the PDE for pressure. Third, update velocity with pressure gradient. In a 2-dimensional domain, the divergence of $\mathbf{u}$ is written as,

%$$ \nabla \cdot \mathbf{u} = \frac{\partial u}{\partial x} + \frac{\partial v}{\partial y}$$

%With central difference in a cell with size of $\Delta x$, it becomes,

%$$ (\nabla \cdot \mathbf{u})_{i, j} = \frac{u_{i+\frac{1}{2}, j} - u_{i-\frac{1}{2}, j}}{\Delta x} + \frac{v_{i, j+\frac{1}{2}} - v_{i, j-\frac{1}{2}}}{\Delta x}. $$

%For each cell there's a similar linear equation with one variable for pressure. Then the discretized Poisson equation is,

%$$ \left( \frac{4p_{i, j} - p_{i+1, j} - p_{i, j+1} - p_{i-1, j} - p_{i, j-1}}{\Delta x^2} \right)  = -\frac{\rho}{\Delta t} \left( \frac{u_{i+\frac{1}{2}, j} - u_{i-\frac{1}{2}, j}}{\Delta x} + \frac{v_{i, j+\frac{1}{2}} - v_{i, j-\frac{1}{2}}}{\Delta x}   \right)$$

%which can be put into matrix form: $Ap = d$, with A being sparse and symmetric. Each row of A corresponds to one cell $(i, j)$, and in 2D case, only 5 entries per row are non-zero. Normally the diagonal entries are non-zero, and off-diagonal entries are either zero or $-1$ at neighboring cells. By solving this linear system, pressure data is updated during each timestep.

Regular grid-based solutions are widely used. But it is not always effective -- a regular equidistant grid gets problematic when boundary conditions focus on an area smaller than the cell itself. Especially with thin boundaries, a large grid solution cannot fully distinct inflow and outflow across boundary, which results in flow artifacts from opposite sides. 

One way to fix this problem is to increase grid resolution in boundary areas until cells become smaller than boundaries, but it's expensive and impractical for fast computations, especially when boundary condition is not constant, i.e. when object is moving. Another approach is the Cut-Cell methods introduced by V. Azevedo et.al. \cite{azevedo2016preserving} to improve performance of grid system.

% set counter to n-1:
\setcounter{chapter}{0}

\chapter{Introduction}

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{figures/gtav}
	\caption[Screenshot from game \textit{Grand Theft Auto V}.]{A screenshot from game \textit{Grand Theft Auto V}, in which a tsunami hits the virtual city Los Santos. Water flows under the bridge and interacts with solid objects such as cars and wire poles, pushing cars above from ground.}
	\label{fig:gtav}
\end{figure}

Fluid simulation in computer graphics is increasingly important for its wide application to both on-line and off-line visual effects generation. Nowadays, complex fluid simulations like gas, water and sand are consolidated in movies, games and industrial applications. Compared to manual parameterization, programmatic generation of fluid dynamics can be much aster and realistic when dealing with different environmental settings. For example, in game industry, a need of algorithmic fluid simulation with faster, larger-scale computation and less resource usage is permanently present. Figure \ref{fig:gtav} \cite{gtav} shows an example of wave simulation in \textit{Grand Theft Auto V}, where all water motions are generated in real-time with one single graphics card. In this scene, the fluid not only follows gravity and large-scale motions, but also interacts with solid objects like cars and wire poles.

For entertainment industry and artistic design, stability and fast computation with user-interactive speed is required for designers to quickly implement dynamical scenes. Figure \ref{fig:3dmaxfluid} \cite{3dmax} shows a screenshot from 3D design and simulation software \textit{Autodesk 3ds Max}, as an example of interactive design with fluid simulation. The designer manually sets up parameters for the fluid, manipulating boundary conditions and initial fluid shapes interactively.  Once the setup is ready, the software generates fluid motion based on a Navier-Stokes solver, splitting all motions into subsequent frames. During design and simulation, the fluid keeps being interactive for scenes that are relatively simple.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1.0\textwidth]{figures/3dmaxfluid}
	\caption[Screenshot from 3D design and simulation software \textit{Autodesk 3ds Max}.]{A screen-shot from 3D design and simulation software \textit{Autodesk 3ds Max}. User manually sets up some physically-meaningful parameters for the fluid, then the software generates fluid motion based on a simple Navier-Stokes solver. During design and simulation, the fluid is always interactive.}
	\label{fig:3dmaxfluid}
\end{figure}

The Navier-Stokes equations are the fundamental mathematical model which describes fluids in a classic Newtonian setting. However, the analytical solution to this complex combination of partial differential equations with tangled boundary conditions is infeasible for most practical usage cases, specially in 3D. Thus, applications in graphics rely on numerical solvers that approximate the solutions of the Navier-Stokes equations in an iterative fashion, often discretized by a regular grid representing the simulation domain. However, most grid-based fluid solvers suffer from instabilities when dealing with thin objects or narrow gaps.  

Recently, a cut-cell approach \cite{azevedo2016preserving} was proposed to deal with thin gaps and intricate boundary conditions in a regular-grid setting. The cut-cell method intersects the original solid object boundaries with regular grid-cells, splitting regular cells into potentially multiple polygonal cells. Compared with regular grids approaches, cut-cells are better able to model sub-grid geometry while preserving proper domain topology.

The cut-cell approach avoids thin gap handling issues in regular Cartesian grids for ordinary fluid solvers, while also having a good capability of handling solid-fluid coupling for both free-slip and no-slip cases. It has minimum modification on Poisson matrix assembly and preserves its sparse and symmetric structure, not requires any increased computations for solving the Poisson equation. However, this is possible due to the assumption that cut-cells pressures are located in the center of regular grid cells, decreasing accuracy of pressure gradient calculation to first-order. This impacts the solution of equations close to boundaries, yielding potentially stagnant regions on narrow gaps due to under-resolved flow artifacts.

Thus, in this report we propose a modification to the original cut-cell method to obtain second-order accuracy when discretizing pressure gradients. We adapt and modify a second-order Poisson solver by H. Ji et al.\cite{ji2006efficient} to our cut-cells setting. Our modifications allow the matrix to maintain the same symmetric positive definite structure as the original cut-cell method of \cite{azevedo2016preserving}, while achieving a better accuracy. This is possible due an modification to the right-hand side of the equations to account for more accurate pressure gradients. We believe that the proposed method in this report will bring better performance both visually and computationally to the original Cut-Cell solver, and is practical for all solvers in the cut-Cell family.


%we extended the increasingly popular Cut-Cell implementation by V. Azevedo et.al.\cite{azevedo2016preserving} and improved its accuracy to second-order, and further preserves the original fluid incompressibility by enforcing the finite volume discretization. To construct the Cut-Cell grid, we clip the fine solid mesh against regular Cartesian grid, then generate polygonal sub-mesh based on intersection nodes. The irregularity of Cut-Cell grid requires high-order modifications on both particle advection and pressure projection. } With the mesh given, we utilize polygonal properties for a higher-order discretization of pressure and pressure gradient in sub-cells, while using a PIC/FLIP-based interpolation for velocity at node locations. For different boundary conditions, different interpolation policies are applied to dictate solid-fluid coupling conditions especially at sub-cell boundaries.



The structure of this report is described as follows: In Chapter 2, related works on grid-based methods and cut-cell fluid solvers are introduced. Chapter 3 first presents a physical and algorithmic perspective on Navier-Stokes equations. We also derive a complete mathematical model to the second-order accurate assembling scheme for Poisson matrix and divergence calculation in each cut-cell. Moreover, solver implementation details are described. Chapter 4 presents simulation results and related analysis with different boundary condition settings to demonstrate accuracy during different simulations scenarios. Finally, Chapter 5 gives our conclusion and future work. 